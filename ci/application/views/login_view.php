<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Login</title>
</head>
<body>
<div id="login_box">
  <h1>Login</h1>
  <?php
  $attributes = array('name'=>'login_form','id'=>'login_form');
  echo form_open('login/process_login',$attributes);
  ?>
  <?php
    $message = $this->session->flashdata('message');
    echo $message == ''? '':'<p id="message">'.$message.'</p>';
  ?>
  <p>
  <label for="username">Username:</label>
  <input autofocus type="text" name="username" size="20" class="form_field" value="<?php echo set_value('username'); ?>" id="" />
  </p>
  <?php echo form_error('username','<p class="field_error">','</p>'); ?>
  <p>
    <input type="submit" name="submit" id="submit" value="Login" />
    <a href="../home">lihat tabel</a>
  </p>
  </form>
</div>
</body>
</html>