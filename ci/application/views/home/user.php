<?php
echo !empty($h2_title)?'<h2>'.$h2_title.'</h2>':'';
echo !empty($message)?'<p class="message">'.$message.'</p>':'';
$flashmessage=$this->session->flashdata('message');
$attributes = array('name'=>'search_form','id'=>'search_form');
echo form_open('home/search',$attributes);
?>
<p>
  <input type="text" name="search" size="20" class="form_field" value="<?php echo set_value('search'); ?>" id="search" />
</p>
<?php echo form_error('searchname','<p class="field_error">','</p>'); ?>
<p>
  <input type="submit" name="submit" id="submit" value="Search" />
</p>
<?php
echo !empty($flashmessage)?'<p class="message">'.$flashmessage.'</p>':'';
echo !empty($pagination)?'<p id="pagination">'.$pagination.'</p>':'';
echo !empty($table)?$table:'';
if (!empty($link))
{
  echo '<p id="bottom_link">';
  foreach ($link as $links)
  {
    echo $links.' ';
  }
  echo '</p>';
}

?>
