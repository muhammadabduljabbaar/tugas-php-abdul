<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title><?php echo isset($title)?$title:'';?></title>
</head>
<body>
  <div id="main">
    <?php $this->load->view($main_view); ?>
  </div>
</body>
</html>