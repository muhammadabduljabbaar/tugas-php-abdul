<?php
class Login extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Login_model','',TRUE);
  }

  public function index()
  {
    if ($this->session->userdata('login')==TRUE)
    {
      redirect('home');
    }else
    {
      $this->load->view('login_view');
    }
  }
  public function process_login()
  {
    $this->form_validation->set_rules('username','Username','required');
    if ($this->form_validation->run()==TRUE)
    {
      $username = $this->input->post('username');
      if ($this->Login_model->check_user($username)==TRUE)
      {
        $data=array('username'=>$username,'login'=>true);
        $this->session->set_userdata($data);
        redirect('home');
      }else
      {
        $this->session->set_flashdata('message','Maaf, username atau password anda salah.');
        redirect('login/index');
      }
    }else
    {
      $this->load->view('login_view');
    }
  }
  public function process_logout()
  {
    $this->session->sess_destroy();
    redirect('login','refresh');
  }
}
?>