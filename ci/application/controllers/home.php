<?php
class home extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Home_model','',true);
  }
  var $title='Home',
    $limit='5';
  function index()
  {
    $this->get_last_ten_user();
  }
  function get_last_ten_user()
  {
    $data['title']=$this->title;
    if ($this->session->userdata('login')==TRUE)
    {
      $user=$this->session->userdata('username');
    }else
    {
      $user='Guest';
    }
    $data['h2_title']='Welcome, '.$user;
    $data['main_view']='home/user';
    $uri_segment=3;
    $offset=$this->uri->Segment($uri_segment);
    $users=$this->Home_model->get_last_ten_user($this->limit,$offset)->result();
    $num_rows=$this->Home_model->count_all_num_rows();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('home/get_last_ten_user');
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="1" cellspacing="1">',
        'row_alt_start'=>'<tr class="zebra">',
        'row_alt_end'=>'</tr>'
        );
      $this->table->set_template($tmpl);
      $this->table->set_empty('&nbsp;');
      $this->table->set_heading('No','Nama','Aksi');
      $i=0+$offset;
      foreach ($users as $user)
      {
        $this->table->add_row(++$i,$user->name,anchor('home/update/'.$user->id,'update',array('class'=>'update')).' '.anchor('home/delete/'.$user->id,'hapus',array('class'=>'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")));
      }
      $data['table']=$this->table->generate();
    }
    else
    {
      $data['message']='Tidak ditemukan satupun data absen';
    }
    $data['link']=array('link_add'=>anchor('home/add','tambah_data',array('class'=>'add')));
    if ($this->session->userdata('login')==TRUE)
    {
      $data['link']['link_logout']=anchor("login/process_logout",'Logout',array('onclick'=>"return confirm('Anda yakin akan logout')"));
    }else
    {
      $data['link']['link_login']=anchor("login/",'Login',array('class'=>"login"));
    }
    $this->load->view('template',$data);
  }
  function delete($id)
  {
    if ($this->session->userdata('login')==true)
    {
      $this->Home_model->delete($id);
      $this->session->set_flashdata('message','1 data user berhasil dihapus');
      redirect('home');
    }
    else
    {
      $this->session->set_flashdata('message','Kamu belum login');
      redirect('home');
    }
  }
  function add()
  {
    if ($this->session->userdata('login')==true)
    {
      $data['title']=$this->title;
      $data['h2_title']='Absen > Tambah Data';
      $data['main_view']='home/user_form';
      $data['link']=array('link_back'=>anchor('home/','kembali',array('class'=>'back')));
      $data['form_action']=site_url('home/add_process');
      $this->load->view('template',$data);
    }
    else
    {
      $this->session->set_flashdata('message','Kamu belum login');
      redirect('home');
    }
  }

  function add_process()
  {
    $data['title']=$this->title;
    $data['h2_title']='Home > Tambah Data';
    $data['main_view']='home/user_form';
    $data['form_action']=site_url('home/add_process');
    $data['link']=array('link_back'=>anchor('home/','kembali',array('class'=>'back')));
    $user=array(
      'name'=>$this->input->post('name')
      );
    $this->Home_model->add($user);
    $this->session->set_flashdata('message','Satu data user berhasil disimpan!');
    redirect('home');
  }
  function update($id)
  {
    if ($this->session->userdata('login')==true)
    {
      $data['title']=$this->title;
      $data['h2_title']='Home > Ubah Data';
      $data['main_view']='home/user_form';
      $data['form_action']=site_url('home/update_process');
      $data['link']=array('link_back'=> anchor('home','kembali',array('class'=>'back')));
      $user=$this->Home_model->get_user_by_id($id)->row();
      $this->session->set_userdata('id',$user->id);
      $data['default']['name']=$user->name;
      $this->load->view('template',$data);
    }
    else
    {
      $this->session->set_flashdata('message','Kamu belum login');
      redirect('home');
    }
  }
  function update_process($id)
  {
    $data['title']=$this->title;
    $data['h2_title']='Home > Ubah Data';
    $data['main_view']='home/user_form';
    $data['form_action']=site_url('home/update_process');
    $data['link']=array('link_back'=>anchor('home/','kembali',array('class'=>'back')));
    $user=array(
      'name'=>$this->input->post('name')
    );
    $this->Home_model->update($this->session->userdata('id'),$user);
    $this->session->set_flashdata('message','Satu data user berhasil di update!');
    redirect('home');
  }
  function search()
  {
    $this->get_match_ten_user();
  }
  function get_match_ten_user()
  {
    $search=array(
      'name'=>$this->input->post('search')
    );
    $data['title']=$this->title;
    if ($this->session->userdata('login')==TRUE)
    {
      $user=$this->session->userdata('username');
    }else
    {
      $user='Guest';
    }
    $data['h2_title']='Welcome, '.$user;
    $data['main_view']='home/user';
    $uri_segment=3;
    $offset=$this->uri->Segment($uri_segment);
    $users=$this->Home_model->get_match_ten_user($search,$this->limit,$offset)->result();
    $num_rows=$this->Home_model->count_all_num_search_rows($search,$this->limit,$offset);
    //~ echo '<pre>'.print_r($num_rows,1).'</pre>';exit();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('home/get_match_ten_user');
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="1" cellspacing="1">',
        'row_alt_start'=>'<tr class="zebra">',
        'row_alt_end'=>'</tr>'
        );
      $this->table->set_template($tmpl);
      $this->table->set_empty('&nbsp;');
      $this->table->set_heading('No','Nama','Aksi');
      $i=0+$offset;
      foreach ($users as $user)
      {
        $this->table->add_row(++$i,$user->name,anchor('home/update/'.$user->id,'update',array('class'=>'update')).' '.anchor('home/delete/'.$user->id,'hapus',array('class'=>'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")));
      }
      $data['table']=$this->table->generate();
    }
    else
    {
      $data['message']='Tidak ditemukan satupun data absen';
    }
    $data['link']=array('link_add'=>anchor('home/add','tambah_data',array('class'=>'add')));
    if ($this->session->userdata('login')==TRUE)
    {
      $data['link']['link_logout']=anchor("login/process_logout",'Logout',array('onclick'=>"return confirm('Anda yakin akan logout')"));
    }else
    {
      $data['link']['link_login']=anchor("login/",'Login',array('class'=>"login"));
    }
    $this->load->view('template',$data);
  }
}
?>