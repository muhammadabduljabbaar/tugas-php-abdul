<?php
class Home_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  var $table = 'users';
  function count_all_num_rows()
  {
    return $this->db->count_all($this->table);
  }
  function get_last_ten_user($limit,$offset)
  {
    $this->db->select('id,name');
    $this->db->from('users');
    $this->db->order_by('created_at','desc');
    $this->db->limit($limit,$offset);
    return $this->db->get();
  }
  function count_all_num_search_rows($search,$limit,$offset)
  {
    $name=$search['name'];
    $this->db->select('id,name');
    $this->db->from('users');
    $this->db->order_by('created_at','desc');
    $this->db->like('name',$name);
    $query= $this->db->get();
    return $query->num_rows();
  }
  function delete($id)
  {
    $this->db->where('id',$id);
    $this->db->delete($this->table);
  }
  function add($user)
  {
    $this->db->insert($this->table,$user);
  }
  function get_user_by_id($id)
  {
    $this->db->select('id,name');
    $this->db->where('id',$id);
    return $this->db->get($this->table);
  }
  function update($id,$user)
  {
    $this->db->where('id',$id);
    $this->db->update($this->table,$user);
  }
  function get_match_ten_user($search,$limit,$offset)
  {
    $name=$search['name'];
    $this->db->select('id,name');
    $this->db->from('users');
    $this->db->order_by('created_at','desc');
    $this->db->like('name',$name);
    $this->db->limit($limit,$offset);
    return $this->db->get();
  }
}
?>