<?php
class Post_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  public function add($post) {
    $this->db->insert('post',$post);
    return $this->db->insert_id();
  }
  var $table='post';
  function count_all_num_rows()
  {
    return $this->db->count_all($this->table);
  }
  function get_post_by_id($pid)
  {
    $this->db->from('post');
    $this->db->join('user','post.uid = user.uid');
    $this->db->where('pid',$pid);
    return $this->db->get();
  }
  function get_last_ten_post($limit,$offset)
  {
    //SELECT post.*, user.nama FROM user, post WHERE post.uid = user.uid
    $this->db->from('post');
    $this->db->join('user','post.uid = user.uid');
    $this->db->order_by('tanggal_buat','desc');
    $this->db->limit($limit,$offset);
    return $this->db->get();
  }
  function update($pid, $post)
  {
    $this->db->where('pid', $pid);
    $this->db->update($this->table, $post);
  }
  function delete($pid)
  {
    return $this->db->delete($this->table, array('pid' => $pid));
  }
}
?>