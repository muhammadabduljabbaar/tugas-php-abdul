<?php
class Login_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  function check($user)
  {
    $un = $user['username'];
    $pw = $user['password'];
    $this->db->from('user');
    $this->db->where('username',$un);
    $result = $this->db->count_all_results();
    $reg = anchor('register', 'registrasi');
    $log = anchor('login', 'login');
    if ($result)
    {
      $this->db->from('user');
      $this->db->select('password');
      $this->db->where('username',$un);
      $result = $this->db->get()->result();
      if ($pw == $result[0]->password)
      {
        return true;
      } else {
        $this->session->set_flashdata('message','Password anda salah, silahkan coba '.$log.' lagi disini');
        return false;
      }
    } else {
      $this->session->set_flashdata('message','Username ini tidak terdaftar, silahkan '.$reg.' disini');
      return false;
    }
    return $this->db->get();
  }
  function get_id_by_un($un)
  {
    $this->db->select('uid');
    $this->db->from('user');
    $this->db->where('username',$un);
    $result = $this->db->get()->result();
    return $result[0]->uid;
  }
}
?>