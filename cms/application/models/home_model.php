<?php
class Home_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  var $table='post';
  function get_last_ten_post($limit,$offset)
  {
    //SELECT post.*, user.nama FROM user, post WHERE post.uid = user.uid
    $this->db->from('post');
    $this->db->join('user','post.uid = user.uid');
    $this->db->order_by('tanggal_buat','desc');
    $this->db->limit($limit,$offset);
    return $this->db->get();
  }
  function count_all_num_rows()
  {
    return $this->db->count_all($this->table);
  }
  function get_last_ten_post_search($limit,$offset)
  {
    $words = $this->uri->segment(3);
    $words = urldecode($words);
    $words = str_replace(',',' ',$words);
    $words = explode(' ',$words);
    $realwords = array();
    foreach ($words as $word)
    {
      if(!empty($word)){
        array_push($realwords, $word);
      }
    }
    //SELECT post.*, user.nama FROM user, post WHERE post.uid = user.uid
    $this->db->from('post');
    $this->db->join('user','post.uid = user.uid');
    $this->db->order_by('tanggal_buat','desc');
    foreach($realwords as $word){
      $this->db->or_like('isi', $word);
    }
    foreach($realwords as $word){
      $this->db->or_like('judul', $word);
    }
    $this->db->limit($limit,$offset);
    return $this->db->get();
  }
  function count_all_num_rows_search()
  {
    $words = $this->uri->segment(3);

    $words = urldecode($words);
    $words = str_replace(',',' ',$words);
    $words = explode(' ',$words);
    $realwords = array();
    foreach ($words as $word)
    {
      if(!empty($word)){
        array_push($realwords, $word);
      }
    }
    foreach($realwords as $word){
      $this->db->or_like('isi', $word);
    }
    foreach($realwords as $word){
    $this->db->or_like('judul', $word);
    }
    return $this->db->count_all_results($this->table);
  }
}
?>