<?php
class Guestbook_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  public $table = 'guestbook';
  public function count_all_num_rows()
  {
    return $this->db->count_all($this->table);
  }
  public function show_recent_guest($limit, $offset)
  {
    $this->db->from($this->table);
    $this->db->order_by('waktu','desc');
    $this->db->limit($limit,$offset);
    return $this->db->get();
  }
  public function add($msg)
  {
    $this->db->insert($this->table,$msg);
    return $this->db->insert_id();
  }
  public function delete($id)
  {
    return $this->db->delete($this->table, array('id' => $id));
  }
}