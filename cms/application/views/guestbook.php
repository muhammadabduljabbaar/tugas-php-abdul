<?php echo form_open('guestbook'); ?>

<h5><label for="name">Nama*</label></h5>
<p class="error"><?php echo form_error('name'); ?></p>
<input type="text" id="name" name="name" value="<?php echo set_value('name'); ?>" size="50" />

<h5><label for="email">Email</label></h5>
<p class="error"><?php echo form_error('email'); ?></p>
<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" />

<h5><label for="isi">Isi</label></h5>
<p class="error"><?php echo form_error('isi'); ?></p>
<textarea name="isi" cols="50" rows="10" id="isi" ><?php echo set_value('isi'); ?></textarea>

<div><input type="submit" value="Kirim" /></div>

</form>