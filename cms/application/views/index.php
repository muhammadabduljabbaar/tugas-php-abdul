<?php echo $header; ?>
<div class="wrapper">
  <h1><?php echo $h1; ?></h1>
  <?php
    echo form_open('home/find', array('class' => 'searchform'));
  ?>
  <input type="text" name="search" value="" id="search" placeholder="search all posts" />
  <input type="submit" name="" value="Search" id="searchbtn" />
  </form>
  <ul class="menu">
  <?php
    foreach($link as $link1) {
      echo "<li>".$link1."</li>";
    }
  ?>
  </ul>
  <h2>Welcome <?php echo $user; ?>!</h2>
  <?php
  echo isset($message)? '<p class="message">'.$message.'</p>':'';
  $flashmessage=$this->session->flashdata('message');
  echo !empty($flashmessage)?'<p class="message">'.$flashmessage.'</p>':'';
  echo isset($pagination)?'<div class="pagination">'.$pagination.'</div>':'';
  ?>
  <div class="container">
    <?php echo $content; ?>
  </div>
  <?php echo isset($pagination)?'<div class="pagination">'.$pagination.'</div>':'';?>
</div>
<?php echo $footer; ?>