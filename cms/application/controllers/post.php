<?php
class post extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Post_model','',true);
  }
  public $table = "post",
    $limit = 10;
  function index()
  {
    //~ create link / menu
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('post/add/','New Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $this->session->set_flashdata('message','Kamu harus Login!');
      redirect('home');
    }
    //~ tampilin post terbaru
  /* Tampilin latest post sesuai limit
   */
    $uri_segment=3;
    $offset=$this->uri->segment($uri_segment);
    $posts=$this->Post_model->get_last_ten_post($this->limit,$offset)->result();
    $num_rows=$this->Post_model->count_all_num_rows();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('post/index');
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="0" cellspacing="0">',
        'row_alt_start'=>'<tr class="posts">',
        'row_alt_end'=>'</tr>'
        );
      $data['content'] = '';
      $this->table->set_template($tmpl);
      $this->table->set_empty("&nbsp;");
      $this->table->set_heading('No', 'Judul', 'Isi', 'Penulis', 'Tanggal', 'Gambar', 'Aksi');
      $i=0+$offset;
      foreach ($posts as $post)
      {
        $this->table->add_row(++$i,anchor('post/view/'.$post->pid,strlen($post->judul)>30?substr($post->judul,0,30).'...':$post->judul),strlen($post->isi)>30?substr($post->isi,0,30).'...':$post->isi,$post->nama,$post->tanggal_buat,$post->gambar, anchor('post/update/'.$post->pid, 'Edit')." ".anchor('post/delete/'.$post->pid, 'Hapus'));
      }
      $data['content']=$this->table->generate();
    }
    else
    {
      $data['content']='Tidak ditemukan satupun post';
    }

    $data['title'] = 'All Post';
    $data['h1'] = 'All Post';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  function add()
  {
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $this->session->set_flashdata('message','Kamu harus Login!');
      redirect('home');
    }
    $this->form_validation->set_rules('judul','Judul Post','required');
    $this->form_validation->set_rules('isi','Content','required');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    //~ $this->form_validation->set_rules('userfile','File');
		if (!$this->form_validation->run())
		{
			$data['content']=$this->load->view('post', $data, true);
		}
    else {
      $config['upload_path'] = './uploads/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']	= '100';
      $config['max_width']  = '1024';
      $config['max_height']  = '768';
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload())
      {
        $error = $this->upload->data();
        if ($error['file_name'] == '')
        {
          $post =array(
            'uid' => '',
            'judul' =>$this->input->post('judul'),
            'isi' =>$this->input->post('isi'),
            'gambar' => '',
            'uid' => $this->session->userdata('uid')
          );
          $id = $this->Post_model->add($post);
          $this->session->set_flashdata('message','Post berhasil di publish.');
          redirect('post/view/'.$id);
        } else {
          $data['error'] = $this->upload->display_errors('<p class="error">','</p>');
          $this->session->set_flashdata('message','Post gagal di publish. Field tidak lengkap.');
          $data['content']=$this->load->view('post', $data, true);
        }
      } else {
        $post =array(
          'uid' => '',
          'judul' =>$this->input->post('judul'),
          'isi' =>$this->input->post('isi'),
          'gambar' => $this->upload->data()['orig_name'],
          'uid' => $this->session->userdata('uid')
        );
        $id = $this->Post_model->add($post);
        $this->session->set_flashdata('message','Post berhasil di publish.');
        redirect('post/view/'.$id);
      }
    }
    $data['content']=$this->load->view('post', $data, true);
    $data['title'] = 'Add Post';
    $data['h1'] = 'Add Post';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  function view($pid)
  {
    //~ create link / menu
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('post/add/','New Post',array('class'=>'menu')));
      array_push($data['link'],anchor('post/update/'.$pid,'Edit This Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $this->session->set_flashdata('message','Kamu harus Login!');
      redirect('home');
    }
    $posts = $this->Post_model->get_post_by_id($pid)->result();
    if (!isset($posts[0]))
    {
      show_error("Tidak ada post ini.");
    }
    $post=$posts[0];
    $data['author'] = $post->nama;
    $data['judul'] = $post->judul;
    $data['tanggal'] = $post->tanggal_buat;
    $data['isi'] = $post->isi;
    $data['gambar'] = $post->gambar;
    $data['pid'] = $post->pid;
    $data['content']=$this->load->view('single',$data,true);
    $data['title'] = 'Post '.$post->judul;
    $data['h1'] = 'Post - '.$post->judul;
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  function update($pid)
  {
    $data['edit'] = 1;
    $data['pid'] = $pid;
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $this->session->set_flashdata('message','Kamu harus Login!');
      redirect('home');
    }
    $this->form_validation->set_rules('judul','Judul Post','required');
    $this->form_validation->set_rules('isi','Content','required');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    //~ $this->form_validation->set_rules('userfile','File');
		if (!$this->form_validation->run())
		{
			$data['content']=$this->load->view('post', $data, true);
		}
    else {
      $config['upload_path'] = './uploads/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']	= '100';
      $config['max_width']  = '1024';
      $config['max_height']  = '768';
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload())
      {
        $error = $this->upload->data();
        if ($error['file_name'] == '')
        {
          $post =array(
            'judul' =>$this->input->post('judul'),
            'isi' =>$this->input->post('isi'),
            'uid' => $this->session->userdata('uid')
          );
          $this->Post_model->update($pid, $post);
          $this->session->set_flashdata('message','Post berhasil di update.');
          redirect('post/view/'.$pid);
        } else {
          $data['error'] = $this->upload->display_errors('<p class="error">','</p>');
          $this->session->set_flashdata('message','Post gagal di publish. Field tidak lengkap.');
          $data['content']=$this->load->view('post', $data, true);
        }
      } else {
        $post =array(
          'uid' => $pid,
          'judul' =>$this->input->post('judul'),
          'isi' =>$this->input->post('isi'),
          'gambar' => $this->upload->data()['orig_name'],
          'uid' => $this->session->userdata('uid')
        );
        $this->Post_model->update($pid, $post);
        $this->session->set_flashdata('message','Post berhasil di upddate.');
        redirect('post/view/'.$pid);
      }
    }
    $posts = $this->Post_model->get_post_by_id($pid)->result();
    if (!isset($posts[0]))
    {
      show_error("Tidak ada post ini.");
    }
    $post=$posts[0];
    $data['author'] = $post->nama;
    $data['judul'] = $post->judul;
    $data['tanggal'] = $post->tanggal_buat;
    $data['isi'] = $post->isi;
    $data['gambar'] = $post->gambar;
    $data['pid'] = $post->pid;
    $data['content']=$this->load->view('post', $data, true);
    $data['title'] = 'Edit Post';
    $data['h1'] = 'Edit Post';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    //~ echo '<pre>'.print_r($data,1).'</pre>';exit();
    $this->load->view('index',$data);
  }
  function delete($pid)
  {
    if ($this->Post_model->delete($pid))
    {
      $this->session->set_flashdata('message','Post berhasil di hapus!');
      redirect('post/');
    }
  }
}

?>