<?php
class register extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Register_model','',true);
  }
  //~ var $table = 'post',
    //~ $limit = '5';
  function index()
  {
    //~ cek login belum
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $this->session->set_flashdata('message','Logout dari akun ini untuk mendaftar!');
      redirect('home');
    } else {
      $data['user'] = 'Guest';
      array_push($data['link'],anchor('home','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('guestbook','Guest Book',array('class'=>'menu')));
      array_push($data['link'],anchor('login','Login',array('class'=>'menu')));
    }
    $this->form_validation->set_rules('username','Username','required|alpha_numeric|is_unique[user.username]');
    $this->form_validation->set_rules('password','Password','required|matches[cpassword]');
    $this->form_validation->set_rules('cpassword','Konfirmasi Password','required');
    $this->form_validation->set_rules('nama','Nama','required|callback_alpha_spaces');
    if ($this->form_validation->run()==true)
    {
      $user=array(
        'username'=>$this->input->post('username'),
        'password'=>md5($this->input->post('password')),
        'nama'=>$this->input->post('nama')
        );
      $this->Register_model->add($user);
      $this->session->set_flashdata('message','Kamu berhasil daftar!<br />Silahkan Login!');
      redirect('home');
    }
    else
    {
      $data['content'] = $this->load->view('register',$data,true);
    }
    $data['title'] = 'Daftar';
    $data['h1'] = 'Daftar';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  function alpha_spaces($nama) {
    $this->form_validation->set_message('alpha_spaces', 'Nama hanya bisa di isi alphabet dan spasi');
    return ( ! preg_match("/^([a-z_ ])+$/i", $nama)) ? FALSE : TRUE;
  }
}

?>