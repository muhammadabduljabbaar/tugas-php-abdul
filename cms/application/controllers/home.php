<?php
class home extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Home_model','',true);
  }
  var $table = 'post',
    $limit = '4';
  function index()
  {
    //~ create link / menu
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('guestbook','Guest Book',array('class'=>'menu')));
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('post/add/','New Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $data['user'] = 'Guest';
      array_push($data['link'],anchor('guestbook','Guest Book',array('class'=>'menu')));
      array_push($data['link'],anchor('login','Login',array('class'=>'menu')));
      array_push($data['link'],anchor('register','Register',array('class'=>'menu')));
    }
    //~ tampilin post terbaru
  /* Tampilin latest post sesuai limit
   */
    $uri_segment=3;
    $offset=$this->uri->segment($uri_segment);
    $posts=$this->Home_model->get_last_ten_post($this->limit,$offset)->result();
    $num_rows=$this->Home_model->count_all_num_rows();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('home/index');
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="0" cellspacing="0">',
        'row_alt_start'=>'<tr class="zebra">',
        'row_alt_end'=>'</tr>'
        );
      $data['content'] = '';
      foreach ($posts as $post)
      {
        $data['judul'] = $post->judul;
        $data['tanggal'] = $post->tanggal_buat;
        $data['isi'] = $post->isi;
        $data['gambar'] = $post->gambar;
        $data['author'] = $post->nama;
        $data['pid'] = $post->pid;
        $data['content'] .= $this->load->view('single',$data,true);
      }
    }
    else
    {
      $data['content']='Tidak ditemukan satupun post';
    }
    $data['title'] = 'Home';
    $data['h1'] = 'Home';
    $data['home'] = true;
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  function find()
  {
    $word = $this->input->post('search');
    $clean = preg_match('/[^A-Za-z0-9\-,\'\w ]/',$word);
    if ($clean)
    {
      $this->session->set_flashdata('message','* Kami menghilangkan spesial karakter yang tidak diperbolehkan.');
    }
    $cleanwords = preg_replace('/[^A-Za-z0-9\-,\'\w ]/','',$word);
    redirect('home/search/'.$cleanwords);
  }
  function search($words)
  {//~ create link / menu
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('post/add/','New Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $data['user'] = 'Guest';
      array_push($data['link'],anchor('login','Login',array('class'=>'menu')));
      array_push($data['link'],anchor('register','Register',array('class'=>'menu')));
    }
    //~ tampilin post terbaru
  /* Tampilin latest post sesuai limit
   */
    $uri_segment=4;
    $offset=$this->uri->segment($uri_segment);
    $posts=$this->Home_model->get_last_ten_post_search($this->limit,$offset)->result();
    $num_rows=$this->Home_model->count_all_num_rows_search();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('home/search/'.$words);
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="0" cellspacing="0">',
        'row_alt_start'=>'<tr class="zebra">',
        'row_alt_end'=>'</tr>'
        );
      $data['content'] = '';
      foreach ($posts as $post)
      {
        $data['judul'] = $post->judul;
        $data['tanggal'] = $post->tanggal_buat;
        $data['isi'] = $post->isi;
        $data['gambar'] = $post->gambar;
        $data['author'] = $post->nama;
        $data['pid'] = $post->pid;
        $data['content'] .= $this->load->view('single',$data,true);
      }
    }
    else
    {
      $data['content']='Tidak ditemukan satupun post';
    }
    $words = urldecode($words);
    $data['message'] = 'Hasil pencarian dari kata kunci "'.$words.'".';
    $data['title'] = 'Home';
    $data['h1'] = 'Home';
    $data['home'] = true;
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
}

?>