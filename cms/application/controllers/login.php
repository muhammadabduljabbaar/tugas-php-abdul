<?php
class login extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Login_model','',true);
  }
  var $table = 'user';
  function index()
  {
    //~ cek login belum
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      redirect('home');
    } else {
      $data['user'] = 'Guest';
      array_push($data['link'],anchor('home','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('guestbook','Guest Book',array('class'=>'menu')));
      array_push($data['link'],anchor('register','Register',array('class'=>'menu')));
    }
    $this->form_validation->set_rules('username','Username','required|alpha_numeric');
    $this->form_validation->set_rules('password','Password','required');
    if ($this->form_validation->run()==true)
    {
      $un = $this->input->post('username');
      $user=array(
        'username'=>$un,
        'password'=>md5($this->input->post('password'))
        );
      $cek = $this->Login_model->check($user);
      if ($cek)
      {
        $this->session->set_flashdata('message','Kamu berhasil Login!');
        $id = $this->Login_model->get_id_by_un($un);
        $userdata = array(
          'uid'=>$id,
          'username'=>$un,
          'login'=>TRUE);
        $this->session->set_userdata($userdata);
        redirect('home');
      }
      else
      {
        $data['content'] = $this->load->view('login',$data,true);
      }
    }
    else
    {
      $data['content'] = $this->load->view('login',$data,true);
    }
    $data['title'] = 'Login';
    $data['h1'] = 'Login';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  function logout()
  {
    $this->session->sess_destroy();
    $this->session->set_flashdata('message','Kamu berhasil Logout!');
    redirect('home');
  }
}

?>