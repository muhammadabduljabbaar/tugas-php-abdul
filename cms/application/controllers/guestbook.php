<?php
class Guestbook extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Guestbook_model','',true);
  }
  public $limit = 10,
    $table = 'guestbook';
  public function index()
  {
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('guestbook/show','Edit Guest Book',array('class'=>'menu')));
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('post/add/','New Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $data['user'] = 'Guest';
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('login','Login',array('class'=>'menu')));
      array_push($data['link'],anchor('register','Register',array('class'=>'menu')));
    }
    $uri_segment=3;
    $offset=$this->uri->segment($uri_segment);
    $msgs=$this->Guestbook_model->show_recent_guest($this->limit,$offset)->result();
    $num_rows=$this->Guestbook_model->count_all_num_rows();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('guestbook/index');
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="0" cellspacing="0">',
        'row_alt_start'=>'<tr class="zebra">',
        'row_alt_end'=>'</tr>'
        );
      $data['content'] = '';
      foreach ($msgs as $msg)
      {
        $data['nama'] = $msg->name;
        $data['email'] = $msg->email;
        $data['isi'] = $msg->isi;
        $data['waktu'] = $msg->waktu;
        $data['content'] .= $this->load->view('singlemsg',$data,true);
      }
    }
    else
    {
      $data['content']='Tidak ditemukan satupun pesan';
    }
    $this->form_validation->set_rules('name','Name','required|callback_alpha_spaces');
    $this->form_validation->set_rules('isi','Isi','required');
    if ($this->form_validation->run()==true)
    {
      $msg = array(
        'id'=>'',
        'name'=>$this->input->post('name'),
        'email'=>$this->input->post('email'),
        'isi'=>$this->input->post('isi')
        );
      $this->Guestbook_model->add($msg);
      $this->session->set_flashdata('message','Pesan berhasil ditambahkan!');
      redirect('guestbook');
    } else {
      if (!$this->session->userdata('login')==true)
      {
        $data['content'] .= $this->load->view('guestbook',$data,true);
      }
    }
    $data['title'] = 'Guest Book';
    $data['h1'] = 'Guest Book';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  public function show(){
    $data['link']=array();
    if ($this->session->userdata('login')==true)
    {
      $data['user'] = $this->session->userdata('username');
      array_push($data['link'],anchor('home/','Home',array('class'=>'menu')));
      array_push($data['link'],anchor('guestbook/','Guest Book',array('class'=>'menu')));
      array_push($data['link'],anchor('post/','View All Post',array('class'=>'menu')));
      array_push($data['link'],anchor('post/add/','New Post',array('class'=>'menu')));
      array_push($data['link'],anchor('login/logout','Log out',array('class'=>'menu')));
    } else {
      $this->session->set_flashdata('message','Kamu harus Login!');
      redirect('home');
    }
    $uri_segment=3;
    $offset=$this->uri->segment($uri_segment);
    $msgs=$this->Guestbook_model->show_recent_guest($this->limit,$offset)->result();
    $num_rows=$this->Guestbook_model->count_all_num_rows();
    if ($num_rows>0)
    {
      $config['base_url']=site_url('guestbook/delete');
      $config['total_rows']=$num_rows;
      $config['per_page']=$this->limit;
      $config['uri_segment']=$uri_segment;
      $this->pagination->initialize($config);
      $data['pagination']=$this->pagination->create_links();
      $tmpl=array(
        'table_open'=>'<table border="1" cellpadding="0" cellspacing="0">',
        'row_alt_start'=>'<tr class="posts">',
        'row_alt_end'=>'</tr>'
        );
      $data['content'] = '';
      $this->table->set_template($tmpl);
      $this->table->set_empty("&nbsp;");
      $this->table->set_heading('No', 'Nama', 'Email', 'Isi', 'Tanggal', 'Aksi');
      $i=0+$offset;
      foreach ($msgs as $msg)
      {
        $this->table->add_row(++$i, $msg->name, $msg->email, strlen($msg->isi)>30?substr($msg->isi,0,30).'...':$msg->isi , $msg->waktu,anchor('guestbook/delete/'.$msg->id,'delete'));
      }
      $data['content']=$this->table->generate();
    }
    else
    {
      $data['content']='Tidak ditemukan satupun post';
    }

    $data['title'] = 'All Post';
    $data['h1'] = 'All Post';
    $data['header'] = $this->load->view('header',$data,true);
    $data['footer'] = $this->load->view('footer',$data,true);
    $this->load->view('index',$data);
  }
  public function delete($id)
  {
    if($this->Guestbook_model->delete($id)){
      $this->session->set_flashdata('message','Pesan berhasil dihapus!');
      redirect('guestbook/show');
    } else {
      $this->session->set_flashdata('message','Pesan gagal dihapus! Kesalahan di database!');
      redirect('guestbook/show');
    }
  }
  public function alpha_spaces($nama) {
    $this->form_validation->set_message('alpha_spaces', 'Nama hanya bisa di isi alphabet dan spasi');
    return ( ! preg_match("/^([a-z_ ])+$/i", $nama)) ? FALSE : TRUE;
  }
}