<?php
session_start();
define('DBHOST','localhost');
define('DBUSER','root');
define('DBPASS','');
define('DBNAME','latihan');
$db=mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME) or die("Error ".mysqli_error($db));
$message=isset($_GET['message'])?$_GET['message']:'';
$id=isset($_GET['id'])?$_GET['id']:'';
$class=isset($_GET['act'])?$_GET['act']:'';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Profil</title>
  <style type="text/css">
    .ubah,.Ubah{background:#FEF9A8;}
    .hapus{background:#FF9398;}
    .Tambah{background:#A2FF05;}
  </style>
</head>
<body>
<?php

if (isset($_SESSION['user']))
{
  echo 'Hallo '.$_SESSION['user'].'. ';
  echo '<a href="index.php?loggedout=1"><button type="button">Logout</button></a> <br />';
  echo $message.'<br />';
  if(!empty($_GET['act'])){
    $act = $_GET['act'];
    if ($act=='tambah') {
    ?>
<form action="aksi.php" method="post">
  <fieldset>
    <legend>Form Tambah Data</legend>
    <label for="name">Nama</label>
    <input type="text" autofocus name="name" value="" id="name" />

    <input type="submit" value="Tambah"  name="submit"/>
    <a href="profil.php"><button type="button">Batal</button></a>
  </fieldset>
</form>

    <?php
    }
    else{
      $id = $_GET['id'];
      $query="SELECT * FROM users WHERE id=$id";
      $result=mysqli_query($db,$query) or die("Error querying ".mysqli_error($db));
      if(!empty($result) && mysqli_num_rows($result)==1){
        $row=mysqli_fetch_assoc($result);
        foreach($row as $k => $v)
        {
          $$k=mysqli_real_escape_string($db,$v);
        }
      }

      if ($act=='ubah') {
        if ($id == $_SESSION['id'])
        {
          echo '<p class="hapus">Ini adalah id yang kamu gunakan saat ini, Yakin mau mengubah?<br /></p>';
        }
    ?>
<form action="aksi.php" method="post">
  <fieldset>
    <legend>Form Ubah Data</legend>
    <label for="name">Nama</label>
    <input type="text" autofocus name="name" value="<?php echo $name; ?>" id="name" />
    <input type="hidden" name="id" value="<?php echo $id; ?>" id="id" />

    <input type="submit" value="Ubah"  name="submit"/>
    <a href="profil.php"><button type="button">Batal</button></a>
  </fieldset>
</form>
    <?php
    }
      elseif ($act=='hapus') {
        if ($id == $_SESSION['id'])
        {
          echo '<p class="hapus">Ini adalah id yang kamu gunakan saat ini, Login menggunakan id lain untuk menghapus id ini.<br /></p>';
        }
        else
        {
    ?>
<form action="aksi.php" method="post">
  <fieldset>
    <legend>Form Hapus Data</legend>
    <p>Nama: <?php echo $name; ?></p>
    <input type="hidden" name="id" value="<?php echo $id; ?>" id="id" />
    <p>Apakah anda yakin mau menghapus data ini?<br /></p>
    <input type="submit" value="Hapus"  name="submit"/>
    <a href="profil.php"><button type="button">Batal</button></a>
  </fieldset>
</form>

    <?php
        }
      }
    }
  }
}
else
{
  if(!empty($_GET['act'])){
    $act=$_GET['act'];
    if (in_array($act, array('hapus','ubah','tambah')))
    {
      echo "Aksi ini hanya untuk user yang sudah login. <br />";
    }
  }
  echo 'Kamu belum login <br /><a href="index.php"><button type="button">Login</button></a> <br />';
}
$query="SELECT * FROM users WHERE 1";
$result=mysqli_query($db,$query) or die("Error querying ".mysqli_error($db));
$data='';
if (mysqli_num_rows($result) !=0)
{
  $i=1;
  while ($row=mysqli_fetch_assoc($result))
  {
    $data.='<tr '.($id==$row['id']?'class='.$class:'').'><td>'.$i.'</td><td>'.$row['name'].'</td><td>'.$row['created_at'].'</td><td><a href="profil.php?act=ubah&id='.$row['id'].'&name='.$row['name'].'"><button type="button">Ubah</button></a><a href="profil.php?act=hapus&id='.$row['id'].'&name='.$row['name'].'"><button type="button">Hapus</button></a></td></tr>';
    $i++;
  }
}
  ?>
  <table border=1>
    <thead>
      <tr>
        <th>NO</th><th>Nama</th><th>Tanggal Daftar</th><th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $data; ?>
    </tbody>
  </table>
  <a href="profil.php?act=tambah"><button type="button">Tambah</button></a>
</body>
</html>